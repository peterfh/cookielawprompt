CookieLawPrompt
===============

Starting on May 26th 2011 sites for any organisation based within the UK (even if their site is hosted overseas) must seek consent to store cookies on a user's computer, or device. Failure to comply could result in a fine up to &pound;500,000.

cPrompt
=======

A live demo of the original cPrompt script can be seen [here](http://michaelwright.me/cPrompt), with some extra implementation information.

Who's behind it?
===============
- [@michaelw90](http://twitter.com/michaelw90) - Coding
- [@panda_doodle](http://twitter.com/panda_doodle) - Idea &amp; design.
- peterfh - CookieLawPrompt fork coding and design.

Usage
===============
Include the script `cookie-law-prompt.js` or the minified version `cookie-law-prompt.min.js` and the stylesheet `cookie-law-prompt.css` in your HTML head. You can customize the CSS styles for the items.

Customize the script if you don't like default parameters values:
```
CookieLawPrompt.hideOnAccept = true;

// Show buttons. Maybe you want the YouTube style prompt, which doesn't have a reject button
CookieLawPrompt.showAcceptButton = true;
CookieLawPrompt.showRejectButton = true;

// Button titles
CookieLawPrompt.acceptButtonTitle = 'ACCEPT';
CookieLawPrompt.rejectButtonTitle = 'REJECT';

// The main message text
CookieLawPrompt.promptMessage = 'This site uses cookies to improve our services. More info in';

// The title of the privacy policy link
CookieLawPrompt.privacyPolicyTitle = 'privacy policy';

// Privacy policy URL
CookieLawPrompt.privacyPolicyUrl = 'http://yoursite.com/privacy-policy.html';

// Script cookie expiration times. You can handle different expiration times depending on the user accepting or not cookies
CookieLawPrompt.cookieExpirationOnReject = 10;		// Re-check if cookies are allowed after 10 days
CookieLawPrompt.cookieExpirationOnAccept = 10000;	// Don't re-check if cookies are allowed for the next 10000 days
```

Check if cookies are allowed in the site prior to executing your cookie generating scripts:
```
if(CookieLawPrompt.isCookiesAllowed())
{
	/* Cookie Storing Code Here. For example, Google Analytics script */
}
```

