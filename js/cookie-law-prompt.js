/*
	CookieLawPrompt
	
	Based on:
		Simple Cookie Prompt
		Idea: @panda_doodle - Coded: @michaelw90
		https://github.com/michaelw90/cPrompt
*/

var CookieLawPrompt =
{
	// Constants
	REJECTED:				0,
	IMPLICITLY_ACCEPTED:	1,
	EXPLICITLY_ACCEPTED:	2,
	FIRST_TIME:				3,

	// Parameters
	hideOnAccept: true,
	showAcceptButton: true,
	showRejectButton: true,
	acceptButtonTitle: 'ACEPTAR',
	rejectButtonTitle: 'RECHAZAR',
	promptMessage: 'Utilizamos cookies propias y de terceros para mejorar nuestros servicios mediante el análisis de sus hábitos de navegación. Si cierra este aviso, continúa navegando o permanece en la web, consideraremos que acepta su uso. Puede obtener más información, o bien conocer cómo cambiar la configuración, en nuestra',
	privacyPolicyTitle: 'política de cookies',
	privacyPolicyUrl: '/politica-de-privacidad',
	cookieExpirationOnReject: 10,
	cookieExpirationOnAccept: 10000,
	autoAccept: true,
	autoAcceptTimeout: 30000,

	// Actual state
	state: false,

	// The Prompt Box Div
	promptBox: false,
	
	
	// Initialize
	init: function()
	{
		var state = this.getState();

		if(window.console)
		{
			console.log('State: ' + state)
			console.log('HideOnAccept: ' + this.hideOnAccept);
		}

		// Show prompt
		if((state == this.FIRST_TIME) || (state == this.IMPLICITLY_ACCEPTED) || (state == this.EXPLICITLY_ACCEPTED && !this.hideOnAccept))
		{
			this.loadPrompt(state);
			
			// Accept after time
			if(this.autoAccept)
			{
				var timeout = setTimeout(function(){CookieLawPrompt.accept()}, this.autoAcceptTimeout);
			}
		}
	},

	// Get cookie acceptance state
	getState: function()
	{
		if(this.state === false)
		{
			if(!document.cookie.match(/_CookieLawPrompt=/))
			{
				this.state = this.FIRST_TIME;
				this.setCookie(this.IMPLICITLY_ACCEPTED);
			}
			else if(document.cookie.match(/_CookieLawPrompt=(\d)($|;)/))
			{
				this.state = parseInt(RegExp.$1);
			}
		}

		return this.state;
	},


	// Load prompt
	loadPrompt: function(n)
	{
		// Create prompt box
		this.promptBox = document.createElement('div');

		var buttons = "";

		if(this.showAcceptButton)
		{
			buttons += ' <a href="#" class="button-accept" onclick="CookieLawPrompt.accept();">' + this.acceptButtonTitle + '</a>';
		}

		if(this.showRejectButton)
		{
			buttons += ' <a class="button-reject" href="#" onclick="CookieLawPrompt.reject(0);">' + this.rejectButtonTitle + '</a>';
		}

		with(this.promptBox)
		{
			id = 'CookieLawPrompt';
			innerHTML = '<p>' + this.promptMessage + ' <a href="' + this.privacyPolicyUrl + '" target="_blank">' + this.privacyPolicyTitle + '</a> ' + buttons + '</p>';
		}
		
		// Insert as first body child
		var body = document.body;
		body.insertBefore(this.promptBox, body.firstChild);
	},


	// Accept cookies
	accept: function()
	{
		var expirationDate = new Date();
		expirationDate.setDate(expirationDate.getDate() + this.cookieExpirationOnAccept);

		this.setCookie(this.EXPLICITLY_ACCEPTED, expirationDate.toGMTString());
		this.promptBox.style.display = 'none';
		location.reload(true);
		
		console.log('eo:' + expirationDate.toGMTString());
	},


	// Reject cookies. Will check again in near future
	reject: function()
	{
		var expirationDate = new Date();
		expirationDate.setDate(expirationDate.getDate() + this.cookieExpirationOnReject);

		this.setCookie(this.REJECTED, expirationDate.toGMTString());
		this.promptBox.style.display = 'none';
	},


	// Check if cookies are allowed
	isCookiesAllowed: function()
	{
		this.state = this.getState();

		return (this.state == this.IMPLICITLY_ACCEPTED || this.state == this.EXPLICITLY_ACCEPTED);
	},


	// Set cookie
	setCookie: function(value, expirationDate)
	{
		document.cookie = "_CookieLawPrompt=" + value + ";expires=" + expirationDate + ";path=/";
	},
}


// Initilize on document ready
if(document.addEventListener)
{
	document.addEventListener("DOMContentLoaded", function()
	{
		CookieLawPrompt.init();
	}, false);
}
else if(document.attachEvent)
{
	document.attachEvent("onreadystatechange", function()
	{
		if(document.readyState === "complete")
		{
			CookieLawPrompt.init();
		}
	});
};
